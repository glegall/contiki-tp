#include "mes-voltage.h"

#define HWTEST_GPIO_INPUT            0
#define HWTEST_GPIO_OUTPUT           1

static void config_gpio(uint8_t port, uint8_t pin, uint8_t type);

////////////////////////////////////////////////////////////////////////////////

double resistanceToTemperature(double rMes) {
	// TODO Students: this function needs to be implemented

	return 0.0;
}

double voltageToResistance(double v) {
	// TODO Students: this function needs to be implemented

	return 0.0;
}

////////////////////////////////////////////////////////////////////////////////

void initForVoltageMeasurement() {
	// Set pin A7 to input
	config_gpio(GPIO_A_NUM, 7, HWTEST_GPIO_INPUT);

	// Initialize the ADC
	adc_init();
}

int getRawVoltageMeasurement() {
	ioc_set_over(GPIO_A_NUM, 7, IOC_OVERRIDE_ANA);
	int mes = adc_get(SOC_ADC_ADCCON_CH_AIN7, SOC_ADC_ADCCON_REF_AVDD5,
		SOC_ADC_ADCCON_DIV_512);

	if (mes < 0) {
		mes = 0;
	}
	if (mes > 32767) {
		mes = 32767;
	}

	return mes;
}

double convertRawVoltageToValue(int rawVoltage) {
	double ref = VCC_BOARD;
	double maxRaw = 32767.0;
	return ((double) rawVoltage) * ref / maxRaw;
}

void printDouble(double d) {
	int integer;
	int decimal;

	integer = (int) d;
	decimal = (int) ((d - ((double) integer)) * 100);

	printf("%d.%02d", integer, decimal);
}

static void config_gpio(uint8_t port, uint8_t pin, uint8_t type) {
	GPIO_SOFTWARE_CONTROL(GPIO_PORT_TO_BASE(port), GPIO_PIN_MASK(pin));
	if (type == HWTEST_GPIO_OUTPUT) {
		GPIO_SET_OUTPUT(GPIO_PORT_TO_BASE(port), GPIO_PIN_MASK(pin));
	} else if (type == HWTEST_GPIO_INPUT) {
		GPIO_SET_INPUT(GPIO_PORT_TO_BASE(port), GPIO_PIN_MASK(pin));
	}
}

