
#include "contiki.h"

#include "adc.h"
#include "gpio.h"
#include "ioc.h"
#include "math.h"
#include "soc-adc.h"

#include <stdio.h>

#define R1 9900.0

// TODO Set here the nominal voltage of your board
#define VCC_BOARD 3.128
//#define VCC_BOARD 2.5

#define A1 3.354016 * 0.001
#define B1 2.569850 * 0.0001
#define C1 2.620131 * 0.000001
#define D1 6.383091 * 0.00000001
#define R_REF 10000.0

double resistanceToTemperature(double rMes);

double voltageToResistance(double v);

void initForVoltageMeasurement();

int getRawVoltageMeasurement();

double convertRawVoltageToValue(int rawVoltage);

void printDouble(double d);

